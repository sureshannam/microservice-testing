Feature: Testing two microservices

  Scenario: Fetch department for the user
    Given I have user name
    When I request to fetch user details
    Then I should get user details
    When I have valid user
    Then I should receive valid department
