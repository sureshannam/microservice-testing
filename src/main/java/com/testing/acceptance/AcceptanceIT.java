package com.testing.acceptance;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@CucumberOptions(features = "src/main/resources/features",
        glue = {"com.testing.acceptance.glue"}
)
@RunWith(Cucumber.class)
public class AcceptanceIT {
}
