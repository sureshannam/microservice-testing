package com.testing.acceptance.glue;

import io.cucumber.java8.En;
import io.restassured.response.Response;
import org.hamcrest.CoreMatchers;

import static io.restassured.RestAssured.given;

public class UserStepDefinitions implements En {
    private String userName;
    private Response response;

    public UserStepDefinitions(){
        Given("I have user name", () -> {
            userName = "Bhagat";
        });

        When("I request to fetch user details", () -> {
            response = given()
                    .when()
                    .get("http://localhost:8080/users?firstName=Bhagat&lastName=Singh");

        });

        Then("I should get user details", () -> {
            response.then()
                    .assertThat()
                    .statusCode(200)
                    .body("firstName", CoreMatchers.equalTo("Bhagat"))
                    .body("lastName", CoreMatchers.equalTo("Singh"));
            Thread.sleep(30000);

        });

       When("I have valid user", () -> {
            userName = "Bhagat";
            response = given()
                    .when()
                    .get("http://localhost:8081/department?name="+userName);

        });

        Then("I should receive valid department", () -> {
            response.then()
                    .assertThat()
                    .statusCode(200)
                    .body("department", CoreMatchers.equalTo("Java"));

        });
    }
}
